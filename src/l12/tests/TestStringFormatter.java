package l12.tests;

import java.util.Date;

public class TestStringFormatter {

	public static void main(String[] args) {
		System.out.println("---------------------------------------------");
		String formattedString = String.format("Testam %2$s in %1$s !", "siruri de caractere", "formatare");
		System.out.println(formattedString);
		
		System.out.println("---------------------------------------------");
		formattedString = String.format("Data de astazi este: %tm %1$te,%1$tY", new Date());
		System.out.println(formattedString);
		
		System.out.println("---------------------------------------------");
		formattedString = String.format("Numarul 25 in decimal = %d", 25);
		System.out.println(formattedString);
		formattedString = String.format("Numarul 25 in decimal = %10d", 215);
		System.out.println(formattedString);
	}

}
