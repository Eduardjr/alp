package l10.tests;

import net.datastructures.LinkedBinaryTree;
import net.datastructures.Position;

public class TestNumberTree {

	public static void main(String[] args) {
		// Define&Create: 
		LinkedBinaryTree<Integer> fBTree = new LinkedBinaryTree<Integer>();
		// Feed
		Position<Integer> fRoot = fBTree.addRoot(50);
		// First level
		Position<Integer>[] firstLevel = new Position[2];
		firstLevel[0] = fBTree.addLeft(fRoot, 25);
		firstLevel[1] = fBTree.addRight(fRoot, 75);
		// Second Level
		Position<Integer>[] secondLevel = new Position[4];
		secondLevel[0] = fBTree.addLeft(firstLevel[0], 5);
		secondLevel[1] = fBTree.addRight(firstLevel[0], 35);
		secondLevel[2] = fBTree.addLeft(firstLevel[1], 60);
		secondLevel[3] = fBTree.addRight(firstLevel[1], 90);
		// Third Level
		Position<Integer>[] thirdLevel = new Position[2];
		thirdLevel[0] = fBTree.addLeft(secondLevel[0], 1);
		thirdLevel[1] = fBTree.addRight(secondLevel[0], 10);
		
		System.out.println("Height of " + fRoot.getElement() + " is: " + fBTree.height(fRoot));
		System.out.println("Height of " + firstLevel[0].getElement() + " is: " + fBTree.height(firstLevel[0]));
		System.out.println("Height of " + thirdLevel[1].getElement() + " is: " + fBTree.height(thirdLevel[1]));
		//
		System.out.println(TreeUtils.toStringIndentTree(fBTree));
		System.out.println("-------------------------------------------------------------");
		
		// Manipulate&Process: traversal
		System.out.println("fBTree.preorder : " 	+ TreeUtils.toStringTreePositions(fBTree.preorder()));

		System.out.println("-------------------------------------------------------------");
		Position<Integer> searchedPosition = searchBTreePreOrder(fBTree, 100);
		if(searchedPosition != null)
			System.out.println("Search result: " + searchedPosition.getElement());
		else
			System.out.println("Not found!");
	}
	
	public static <E> Position<E> searchBTreePreOrder(LinkedBinaryTree<E> BTree, E searchElement){
		return searchNode(BTree, BTree.root(), searchElement);
	}
	
	private static <E> Position<E> searchNode(LinkedBinaryTree<E> BTree, Position<E> node, E searchElement){
		/*
		if (node == null)
			return null;
		System.out.println("Searched node element: " + node.getElement());
		*/
		
		if (node.getElement().equals(searchElement))
			return node;
		
		if (BTree.isExternal(node))
			return null;
		
		Comparable<E> comparableSearchElement = (Comparable<E>)  searchElement;
		if (comparableSearchElement.compareTo(node.getElement()) < 0)
			return searchNode(BTree, BTree.left(node), searchElement);
		else
			return searchNode(BTree, BTree.right(node), searchElement);
	}
}
// https://www.baeldung.com/java-string-formatter //
