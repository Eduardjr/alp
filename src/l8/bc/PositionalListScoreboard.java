package l8.bc;

import dsaj.arrays.GameEntry;
import net.datastructures.LinkedPositionalList;
import net.datastructures.Position;
import net.datastructures.PositionalList;

/** <Business-Case-App>
 *  Class for storing high scores in an array in nondecreasing order. 
 * */
public class PositionalListScoreboard {
	// private int numEntries = 0; // number of actual entries
	// private GameEntry[] board; // array of game entries (names & scores)
	private PositionalList<GameEntry> board;
	private int capacity;

	/**
	 * Constructs an empty scoreboard with the given capacity for storing entries.
	 */
	public PositionalListScoreboard(int capacity) {
		// board = new GameEntry[capacity];
		board = new LinkedPositionalList<GameEntry>();
		this.capacity = capacity;
	}

	/** Attempt to add a new score to the collection (if it is high enough) */
	public void add(GameEntry e) {
		int newScore = e.getScore();
		
		// board will add its first element (is empty)
		if (board.isEmpty()) {
			board.addFirst(e);
			return;
		}
		
		// is the new entry e really a high score?
		if (board.size() < this.capacity || newScore > board.last().getElement().getScore()) {
			// start with last position and process backwards
			Position<GameEntry> position = board.last();
			while(position!=null && position.getElement().getScore() < newScore) {
				// board.addAfter(position, position.getElement());
				// previous position to process next
				position = board.before(position);
			}
			if (position != null)
				board.addAfter(position, e);
			else {
				board.addFirst(e);
			}
			// check capacity
			if (this.board.size() > this.capacity)
				this.board.remove(board.last());
				
		}
	}

	/** Remove and return the high score at index i. */
	public GameEntry remove(int i) throws IndexOutOfBoundsException {
		if (i < 0 || i >= board.size())
			throw new IndexOutOfBoundsException("Invalid index: " + i);
		// Get position from i
		Position<GameEntry> position = board.first(); int j=0;
		while(position != null && i > j) {
			j++;
			position = board.after(position);
		}
		GameEntry temp = position.getElement();
		board.remove(position);
		
		return temp; // return the removed object
	}

	/** Returns a string representation of the high scores list. */
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		sb.append(board);
		sb.append("]");
		return sb.toString();
	}

	/* Testing business-case-app
	 * 
	 * */
	public static void main(String[] args) {
		// The main method
		PositionalListScoreboard highscores = new PositionalListScoreboard(5);
		String[] names = { "Rob", "Mike", "Rose", "Jill", "Jack", "Anna", "Paul", "Bob" };
		int[] scores = { 750, 1105, 590, 740, 510, 660, 720, 400 };

		for (int i = 0; i < names.length; i++) {
			GameEntry gE = new GameEntry(names[i], scores[i]);
			System.out.println("Adding " + gE);
			highscores.add(gE);
			System.out.println(" Scoreboard: " + highscores);
		}
		System.out.println("Removing score at index " + 3);
		highscores.remove(3);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
	}
}
