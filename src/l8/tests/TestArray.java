package l8.tests;

import java.util.Arrays;


import dsaj.arrays.GameEntry;
import l8.bc.ArrayScoreboard;

public class TestArray {

	public static void main(String[] args) {
		testArrayOps();
		testArrayScoreboard();
	}
	
	public static void testArrayOps() {
		// Define&Create: ARRAY
		String[] tablou = new String[10]; 
		
		// Feed
		for (int i = 0; i < 10; i++) {
			tablou[i] = "Element_" + i;
		}
		System.out.println("1 ########################");
		System.out.println("tablou: " + Arrays.toString(tablou));
		System.out.println("##########################\n");
		
		
		// Manipulate
		tablou[2] = null;
		tablou[4] = null;
		tablou[5] = "New_Element_5";
		System.out.println("2 ########################");
		System.out.println("tablou: " + Arrays.toString(tablou));
		System.out.println("##########################\n");
		
		
		// Process
		String tablouElement = tablou[5];
		System.out.println("3 ########################");
		System.out.println("tablou[5] = " + tablouElement);
		System.out.println("##########################\n");
	}
	
	static void testArrayScoreboard() {
		// The main method
		ArrayScoreboard highscores = new ArrayScoreboard(5);
		String[] names = { "Rob", "Mike", "Rose", "Jill", "Jack", "Anna", "Paul", "Bob" };
		int[] scores = { 750, 1105, 590, 740, 510, 660, 720, 400 };
		
		
		System.out.println("4 ########################");
		for (int i = 0; i < names.length; i++) {
			GameEntry gE = new GameEntry(names[i], scores[i]);
			System.out.println("Adding " + gE);
			highscores.add(gE);
			System.out.println(" Scoreboard: " + highscores + "\n");
		}
		System.out.println("##########################\n");
		
		
		System.out.println("5 ########################");
		System.out.println("Removing score at index " + 3);
		highscores.remove(3);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("6 ########################");
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("7 ########################");
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("8 ########################");
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("9 ########################");
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("##########################\n");
	}
}