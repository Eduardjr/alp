package l8.ext;

import java.util.Arrays;

public class ArraysExtensionTest {

	public static void main(String[] args) {
		// Define&Create: ARRAY
		String[] tablou = new String[10]; 
		
		// Feed
		for (int i = 0; i < 10; i++) {
			tablou[i] = "Element_" + i;
		}
		System.out.println("1 ########################");
		System.out.println("tablou: " + Arrays.toString(tablou));
		System.out.println("##########################\n");
		
		
		// Add first
		String[] tablouExtins = new String[20];
		for(int i=0; i < tablou.length; i++) {
			tablouExtins[i+1] = tablou[i];
		}
		tablouExtins[0] = "New_Element_0";
		System.out.println("2 ########################");
		System.out.println("tablouExtins: " + Arrays.toString(tablouExtins));
		System.out.println("##########################\n");
		
		
		// Add last
		tablouExtins = Arrays.copyOf(tablou, 20);
		tablouExtins[10] = "Element_" + 10;
		System.out.println("3 ########################");
		System.out.println("tablouExtins: " + Arrays.toString(tablouExtins));
		System.out.println("##########################\n");
		
		
		// Add before
		System.out.println("4 ########################");
		int idx = Arrays.binarySearch(tablou, "Element_7");
		System.out.println(idx + " from " + Arrays.toString(tablou));
		tablouExtins = Arrays.copyOf(tablou, tablou.length + 1);
		for(int i = tablouExtins.length-2; i >= idx; i--) {
			tablouExtins[i+1] = tablouExtins[i];
		}
		tablouExtins[idx] = "New_Element_" + idx;
		System.out.println("tablouExtins: " + Arrays.toString(tablouExtins));
		System.out.println("##########################\n");
		
		
		
		// Add before keep array size
		System.out.println("5 ########################");
		idx = Arrays.binarySearch(tablou, "Element_7");
		System.out.println(idx + " from " + Arrays.toString(tablou));
		for(int i = tablou.length-2; i >= idx; i--) {
			tablou[i+1] = tablou[i];
		}
		tablou[idx] = "New_Element_" + idx;
		System.out.println("tablou: " + Arrays.toString(tablou));
		System.out.println("##########################\n");
		// Add after ?
	}
}
