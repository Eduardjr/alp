package l8.ext;

import net.datastructures.DoublyLinkedList;

public class DoublyLinkedListExtension<E> extends DoublyLinkedList<E>{
	public DoublyLinkedListExtension<E> addBefore(E positionalElement, E e) {
		DoublyLinkedListExtension<E> tempList = new DoublyLinkedListExtension<E>();
		E nextElement;
		do {
			nextElement = this.removeLast();
			tempList.addFirst(nextElement);
			if (nextElement.equals(positionalElement)) {
				System.out.println("Am gasit positionalElement = " + positionalElement);
				tempList.addFirst(e);
			}
			System.out.println("tempList:" + tempList);
		}while(!this.isEmpty());
		return tempList;
	}
	//
	public static void main(String... args) {
		DoublyLinkedListExtension<String> dList = new DoublyLinkedListExtension<String>();
		for (int i = 0; i < 7; i++)
			dList.addLast("Element_" + (i+1));
		System.out.println("pList:" + dList);
		
		dList = dList.addBefore("Element_5", "NEW_Element_5");
		System.out.println("pList:" + dList);
	}
}
