package l9.tests;

import net.datastructures.Map;
import net.datastructures.UnsortedTableMap;

public class TestMaps {

	public static void main(String[] args) { 
		
		// Define&Create: UnsortedTableMap
		Map<Integer, String> aMap = new UnsortedTableMap<>();
		
		Map<String, String> sMap = new UnsortedTableMap<>();
		for (int i = 0; i < 10; i++)
			sMap.put("Key"+ i, "Element_" + i);
		System.out.println("sMap: " + toStringMap(sMap));
		
		// Feed
		for (int i = 0; i < 10; i++)
			aMap.put(i, "Element_" + i);
		System.out.println("aMap: " + toStringMap(aMap));
		
		// Manipulate&Process
		String mapElement = aMap.get(0);
		System.out.println("aMap: " + toStringMap(aMap) + " - contains element: " + mapElement);
		
		mapElement = aMap.get(aMap.size() - 1);
		System.out.println("aMap" + toStringMap(aMap) + " - contains last element: " + mapElement);

		String removedMapElement = aMap.remove(0);
		System.out.println("after remove aMap: " + toStringMap(aMap));
	}
	
	private static <K, V> String toStringMap(Map<K, V> map) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(K k: map.keySet())
			sb.append("(" + k + ", " + map.get(k) + "), ");
		sb.append("]");
		return sb.toString().replace(", ]", "]");
	}
}