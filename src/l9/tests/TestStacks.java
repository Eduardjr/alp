package l9.tests;

import net.datastructures.ArrayStack;
import net.datastructures.LinkedStack;
import net.datastructures.Stack;

public class TestStacks {

	public static void main(String[] args) {
		// Define&Create: ArrayStack
		Stack<String> aStack = new ArrayStack<String>();
		
		// Feed
		for (int i = 0; i < 10; i++)
			aStack.push("Element_" + i);
		System.out.println("aStack: " + aStack);
		
		// Manipulate&Process
		String stackElement = aStack.top();
		System.out.println("aStack: " + aStack + " - contains top element: " + stackElement);
		
		stackElement = aStack.pop();
		System.out.println("aStack: " + aStack + " - contains popped element: " + stackElement);
		
		
		// Define&Create: LinkedStack
		Stack<String> lStack = new LinkedStack<String>();
		
		// Feed
		for (int i = 0; i < 10; i++)
			lStack.push("Element_" + i);
		System.out.println("lStack: " + aStack);
		
		// Manipulate&Process
		stackElement = lStack.top();
		System.out.println("lStack: " + lStack + " - contains top element: " + stackElement);
		
		stackElement = lStack.pop();
		System.out.println("lStack: " + lStack + " - contains popped element: " + stackElement);
	}

}
