package l9.tests;

import java.util.Comparator;
import java.util.Random;

import net.datastructures.ArrayQueue;
import net.datastructures.Entry;
import net.datastructures.LinkedQueue;
import net.datastructures.PriorityQueue;
import net.datastructures.Queue;
import net.datastructures.SortedPriorityQueue;
import net.datastructures.UnsortedPriorityQueue;

public class TestQueues {

	public static void main(String[] args) {
		// Define&Create: ArrayQueue
		Queue<String> aQueue = new ArrayQueue<String>();

		// Feed
		for (int i = 0; i < 10; i++)
			aQueue.enqueue("Element_" + i);
		System.out.println("aQueue: " + aQueue);
		
		// Manipulate&Process
		String queueElement = aQueue.first();
		System.out.println("aQueue: " + aQueue + " - first element: " + queueElement);
		
		queueElement = aQueue.dequeue();
		System.out.println("aQueue: " + aQueue + " - dequeued element: " + queueElement);
		
		
		// Define&Create: LinkedQueue
		Queue<String> lQueue = new LinkedQueue<String>();
				
		// Feed
		for (int i = 0; i < 10; i++)
			lQueue.enqueue("Element_" + i);
		System.out.println("lQueue: " + lQueue);
		
		// Manipulate&Process
		queueElement = lQueue.first();
		System.out.println("lQueue: " + lQueue + " - first element: " + queueElement);
		
		queueElement = lQueue.dequeue();
		System.out.println("lQueue: " + lQueue + " - dequeued element: " + queueElement);
		
		
		
		
		// Define&Create: PriorityQueue
		PriorityQueue<Integer, String> lpQueue = 
				//new UnsortedPriorityQueue<Integer, String>();
				new UnsortedPriorityQueue<>(new ComparatorInversInteger());
				//new SortedPriorityQueue<>(new ComparatorInversInteger());
				
		Entry<Integer, String> pQueueElement;
		Integer pQueueElementPriority = 0;
		StringBuilder sb = new StringBuilder("(");
		// Feed
		Random r = new Random();
		for (int i = 0; i < 10; i++) {
			//pQueueElementPriority = new Double(Math.random() * 100).intValue();
			pQueueElementPriority = r.nextInt(100);
			lpQueue.insert(pQueueElementPriority, "Element_" + i);
			sb.append("(" + pQueueElementPriority + ", " + "Element_" + i + "),");
		}
//		lpQueue.insert(pQueueElementPriority, "Element_" + lpQueue.size());
//		System.out.println(">>> " + pQueueElementPriority + ": " + lpQueue.size());
		//
		sb.append(")");
		System.out.println("lpQueue: " + sb);
		
		// Manipulate&Process
		while(!lpQueue.isEmpty()) {
			// sterge elementul cu prioritatea cea mai mare
			pQueueElement = lpQueue.removeMin();
			System.out.println("removed lpQueue element: " + pQueueElement.getValue() + " with priority: " + pQueueElement.getKey());
		}
	}
	
	static class ComparatorInversInteger implements Comparator<Integer>{

		@Override
		public int compare(Integer i1, Integer i2) {
			return -i1.compareTo(i2);
		}
		
	}
}
/** <Basic-Ops>
- Define&Create
- Feed with elements
  Output state
- Manipulate elements
  Output state
- Process elements
  Output result
**/