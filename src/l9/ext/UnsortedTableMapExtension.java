package l9.ext;

import java.util.Iterator;
import java.util.Random;

import net.datastructures.ArrayList;
import net.datastructures.UnsortedTableMap;

public class UnsortedTableMapExtension<K, V> extends UnsortedTableMap<K,V>{	
	//
	public ArrayList<K> keyBubleSort() {
		ArrayList<K> keyList = keyList();
		System.out.println("keyList: " + keyList);
		if(keyList.isEmpty() || !(keyList.get(0) instanceof Comparable))
			throw new RuntimeException("List is empty or Key is not Comparable");
		//
		boolean ordonat = false;
		K previousKey = null;
		K currentKey = null;
		while(!ordonat) {
			for (int i=1; i < keyList.size(); i++) {
				ordonat = true;
				//
				previousKey = keyList.get(i-1);
				currentKey = keyList.get(i);
				Comparable<K> comparableCurrentKey = (Comparable<K>)currentKey;
				// we assume that keys are Comparable
				if (comparableCurrentKey.compareTo(previousKey) < 0) {
					keyList.set(i-1, currentKey);
					keyList.set(i, previousKey);
					ordonat = false;
				}
			}
		}
		return keyList;
	}
	//
	private ArrayList<K> keyList(){
		ArrayList<K> keyList = new ArrayList<>();
		Iterator<K> keySetIterator = this.keySet().iterator();
		int j = 0;
		while(keySetIterator.hasNext()) {
			keyList.add(j++, keySetIterator.next());
		}
		System.out.println("keyList: " + keyList);
		return keyList;
	}
	//
	public ArrayList<K> keyQuickSort() {
		ArrayList<K> keyList = keyList();
		if(keyList.isEmpty() || !(keyList.get(0) instanceof Comparable))
			throw new RuntimeException("List is empty or Key is not Comparable");
		//
		quickSort(keyList, 0, keyList.size()-1);
		return keyList;
	}
	public void quickSort(ArrayList<K> list, int s, int d) {
		if (s < d) {
			System.out.println("quickSort: " + list);
			int indexPartition = quickSortPartition(list, s, d);
			System.out.println("	quickSort: [" + s + ", " + (indexPartition - 1) + "]");
			quickSort(list, s, indexPartition - 1);
			System.out.println("	quickSort: [" + (indexPartition + 1) + ", " + d + "]");
			quickSort(list, indexPartition + 1, d);
		}
	}
	private int quickSortPartition(ArrayList<K> list, int s, int d) {
		System.out.println("---------------------------------------------------------------");
		System.out.println("    quickSortPartition: [" + s + ", " + d + "]");
		K pivot = list.get(d);
		System.out.println("        pivot: " + pivot);
		int pivotPosition = s - 1;
		Comparable<K> comparableElement = null;
		for(int j = s; j < d; j++) {
			comparableElement = (Comparable<K>)list.get(j);
			if (comparableElement.compareTo(pivot) <= 0) {
				pivotPosition++;
				K temp = list.get(pivotPosition);
				list.set(pivotPosition, list.get(j));
				list.set(j, temp);
				System.out.println("            mutated-list: " + list + ", partition boundary: " + pivotPosition);
			}
		}
		//
		pivotPosition++;
		//
		K temp = list.get(pivotPosition);
		list.set(pivotPosition, pivot);
		list.set(d, temp);
		System.out.println("        pivoted list: " + list + ", pivot position: " + (pivotPosition));
		System.out.println("---------------------------------------------------------------");
		return pivotPosition;
	}
	
	
//	@Override
//	public V get(K key) {
//		return null;
//	}
//	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(K k: this.keySet())
			sb.append("(" + k + ", " + this.get(k) + "), ");
		sb.append("]");
		return sb.toString().replace(", ]", "]");
	}
	
	
	public static void main(String[] args) {
		// Create UnsortedTableMapExtension
		UnsortedTableMapExtension<Integer, String> unsortedMap = new UnsortedTableMapExtension<>();
		Random r = new Random();
		Integer randomKey;
		for (int i=0; i < 10; i++) {
			//randomKey = new Double(Math.random() * 100).intValue() + 1;
			do {
				randomKey = r.nextInt(100);
			}while(unsortedMap.get(randomKey) != null);
			unsortedMap.put(randomKey, "Element_" + i);
		}
		System.out.println("unsortedMap: " + unsortedMap);
		
		ArrayList<Integer> keysBSorted = unsortedMap.keyBubleSort();
		System.out.println("sortedKeys.bubble: " + keysBSorted);
		//
		ArrayList<Integer> keysQSorted = unsortedMap.keyQuickSort();
		System.out.println("sortedKeys.quick: " + keysQSorted);
		
	}
}
/*
Implement Set Operations (union, intersect, minus)
Implement string conversion operation
Implement key-sort operation
[reverse, sort, search, backT, greedyOpt]
*/