package l11.tests;

import net.datastructures.Edge;
import net.datastructures.Graph;
import net.datastructures.PositionalList;
import net.datastructures.Vertex;

public class GraphUtils {
	// Convert vertex into String
	public static <V, E> String toString(Vertex<V> vertex){
		return vertex.getElement().toString();
	}
	// Convert edge into String
	public static <V, E> String toString(Edge<E> edge){
		return edge.getElement().toString();
	}
	// Convert vertex of edge from graph into specific String representation
	public static <V, E> String toString(Vertex<V> vertex, Edge<E> ofEdge, Graph<V,E> fromGraph){
		String representation = vertex.getElement() + "(" 
				+ fromGraph.endVertices(ofEdge)[0].getElement() + "-"
				+ fromGraph.endVertices(ofEdge)[1].getElement() + ","
				+ ofEdge.getElement() 
				+ "),";
		return representation.replace("),",")");
	}
	// Convert all vertexes from edges within graph into specific single String representation
	public static <V, E> String toStringAll(Vertex<V> vertex, 
			PositionalList<Edge<E>> ofEdgePath, 
			Graph<V,E> fromGraph){
		String representation = vertex.getElement() + "[";
		for (Edge<E> e: ofEdgePath) {
			representation += fromGraph.endVertices(e)[0].getElement() + "-"
						   + fromGraph.endVertices(e)[1].getElement() 
						   + "(" + e.getElement() + ")"
						   + ",";
		}
		representation += "]";
		return representation.replace(",]", "]");
	}
}
